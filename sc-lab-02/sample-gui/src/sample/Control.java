package sample;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;

public class Control {
	
	BankAccount bank;
	InvestmentFrame gui ;
	ActionListener list ;
	public Control(){
	   bank = new BankAccount();
	   gui = new InvestmentFrame();
	   gui.pack();
	   gui.setVisible(true);
	   gui.setSize(600, 90);
	   gui.setTitle(" WELCOME to CALCULATE  INTEREST  PROGRAM");
	   gui.setLocation(400,250);
	   list = new AddInterestListener();
	   gui.setListener(list);
	   gui.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
	
	
	public Control(double initialBalance) {
		// TODO Auto-generated constructor stub
		bank = new BankAccount( initialBalance);
	}
	public void deposite(double amount){
		bank.deposit(amount);
	}
	public void withdraw(double amount){
		bank.withdraw(amount);
	}
	
	public double getBalance(){
		 return  bank.getBalance();
	}
	
	 class AddInterestListener implements ActionListener
     {
        public void actionPerformed(ActionEvent event)
        {
           double rate = gui.getInterest();
           double interest = bank.getBalance() * rate / 100;
           bank.deposit(interest);
          gui. setResult("balance: " + bank.getBalance());
        }            
     }
     
}
